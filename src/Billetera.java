
public class Billetera {
	private Dinero saldo;
	
	private Usuario usuario;
	
	/**
	 * El CVU es el identificado de una billetera
	 */
	private Integer cvu;
	
	public Billetera(Integer cvu, Usuario usuario, Dinero saldoInicial) {
		this.cvu = cvu;
		this.usuario = usuario;
		this.saldo = saldoInicial;
	}
	
	public Billetera(Integer cvu, Usuario usuario, Moneda moneda) {
		this(cvu, usuario, new Dinero(moneda));
	}
	
	public void sumarDinero(Dinero dinero) {
		this.saldo = this.saldo.sumar(dinero);
	}
	
	public boolean restarDinero(Dinero dinero) {
		boolean resultado = false;
		if(this.getSaldo().esMayorQue(dinero) || this.getSaldo().equals(dinero)) {
			this.saldo = this.getSaldo().restar(dinero);
			resultado = true;
		}
		return resultado;
	}
	
	public Moneda getMoneda() {
		return this.saldo.getMoneda();
	}
	
	public Dinero getSaldo() {
		return this.saldo;
	}
	
	public Usuario getUsuario() {
		return this.usuario;
	}

	public Integer getCvu() {
		return cvu;
	}

	public void setCvu(Integer cvu) {
		this.cvu = cvu;
	}
	
		
}
