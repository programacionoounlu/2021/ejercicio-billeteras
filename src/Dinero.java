import java.util.Objects;

/**
 * Clase que representa al dinero papel
 */
public class Dinero {
	private Double monto;
	
	private Moneda moneda;
	
	public Dinero(Moneda moneda) {
		// Desde este constructor invoco al otro constructor para
		// evitar duplicar el código
		this(moneda, 0.0);
	}
	
	public Dinero(Moneda moneda, Double monto) {
		this.setMoneda(moneda);
		this.setMonto(monto);		
	}
	
	public Double getMonto() {
		return this.monto;
	}
	
	public Moneda getMoneda() {
		return this.moneda;
	}
	
	private void setMonto(Double monto) {
		if(monto >= 0) {
			this.monto = monto;
		}
	}
	
	private void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}
		
	public Dinero sumar(Double monto) {
		return new Dinero(this.getMoneda(), monto + this.getMonto());
	}
	
	// El método sumar está SOBRECARGADO y puede operar tanto
	// con una instancia de Double como con una instancia de Dinero 
	public Dinero sumar(Dinero dinero) {
		this.verificarMismaMoneda(dinero);
		return new Dinero(this.getMoneda(), this.getMonto() + dinero.getMonto());
	}
	
	public Dinero restar(Double monto) {
		return new Dinero(this.getMoneda(), this.getMonto() - monto);
	}
	
	public Dinero restar(Dinero dinero) {
		this.verificarMismaMoneda(dinero);
		return new Dinero(this.getMoneda(), this.getMonto() - dinero.getMonto());
	}	
	
	// Normalmente el dinero lo multiplicamos o dividimos por un escalar. Por 
	// ejemplo al aplicarle una tasa o dividirlo entre partes. En consecuencia,
	// los métodos multiplicar y dividir solo admiten operar con Double
	public Dinero mutiplicar(Double multiplicador) {
		return new Dinero(this.getMoneda(), this.getMonto() * multiplicador);
	}
	
	public Dinero dividir(Double divisor) {
		return new Dinero(this.getMoneda(), this.getMonto() / divisor);
	}
	
	/**
	 * Este método verifica que la moneda de esta instancia sea igual a
	 * la de otra instancia. Caso contrario, arroja una excepción. 
	 * 
	 * Unaexcepción es un objeto que representa un error o una situación 
	 * excepcional y contiene información acerca de dicho evento o error.
	 * Interrumpe el flujo de ejecución normal del código. Una bloque de 
	 * código que puede producir una excepción debe manejar la misma mediante
	 * un manejador de excepciones o especificar que arroja una excepción.
	 * Aquí vemos el segundo caso, mediante la expresión:
	 * 
	 * 		 "throws IllegalArgumentException"
	 * 
	 * nuestro método está avisando que puede producir una excepción que
	 * representa un argumento no válido fue recibido. 
	 * 
	 * Puedes obtener más información sobre excepciones aquí:
	 * @see https://docs.oracle.com/javase/tutorial/essential/exceptions/index.html
	 * @param otraInstancia
	 */
	private void verificarMismaMoneda(Dinero otraInstancia) throws IllegalArgumentException {
		if(this.getMoneda() != otraInstancia.getMoneda()) {
			// Si las monedas son diferentes "arrojamos" la excepción
			// Observemos que una excepción es un objeto
			throw new IllegalArgumentException(
					"La operación no se puede aplicar porque las monedas no son iguales " +
					this.getMoneda() + " != " + otraInstancia.getMoneda()
					);			
		}
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getMoneda() + " " + this.getMonto();
	}

	@Override
	public int hashCode() {
		return Objects.hash(moneda, monto);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Dinero other = (Dinero) obj;
		return this.getMoneda() == other.getMoneda() && Objects.equals(this.getMonto(), other.getMonto());
	}
	
	public boolean esMenorQue(Dinero dinero) {
		this.verificarMismaMoneda(dinero);
		return this.getMonto() < dinero.getMonto();
	}
	
	public boolean esMayorQue(Dinero dinero) {
		this.verificarMismaMoneda(dinero);
		return this.getMonto() > dinero.getMonto();
	}
		
}
