import java.util.ArrayList;
import java.util.List;

public class Banco {
	private List<Billetera> billeteras;
	
	private Cotizador cotizador;
	
	public Banco() {
		this.billeteras = new ArrayList<>();
		this.cotizador = new Cotizador();
	}
	
	public boolean agregarBilltera(Integer cvu, String apellido, String nombre, Dinero montoInicial) {
		boolean resultado = false;
		if(!this.existeBilletera(cvu)) {
			this.billeteras.add(
					new Billetera(
							cvu, 
							new Usuario(nombre, apellido), 
							montoInicial
							)
					);
			resultado = true;
		}
		return resultado;
	}
	
	public void agregarCotizacion(Moneda de, Moneda a, Double tasa) {
		this.cotizador.agregarCotizacion(new Cotizacion(de, a, tasa));
	}
	
	public Dinero obtenerSaldoBilletera(Integer cvu) {
		Dinero resultado = null;
		if(this.existeBilletera(cvu)) {
			resultado = this.buscarBilletera(cvu).getSaldo();
		}
		return resultado;
	}
	
	public boolean transferir(Integer cvuOrigen, Integer cvuDestino, Double monto) {
		boolean resultado = false;
		Billetera origen = this.buscarBilletera(cvuOrigen);
		Billetera destino = this.buscarBilletera(cvuDestino);
		if(origen != null && destino != null) {
			Dinero dineroOrigen = new Dinero(origen.getMoneda(), monto);
			Dinero dineroDestino = null;
			if(origen.getMoneda() != destino.getMoneda()) {
				if(this.cotizador.puedeCotizar(origen.getMoneda(), destino.getMoneda())) {
					dineroDestino = this.cotizador.convertir(dineroOrigen, destino.getMoneda());
				}
			}else {
				dineroDestino = new Dinero(destino.getMoneda(), monto);
			}
			if(dineroDestino != null) {
				resultado = origen.restarDinero(dineroOrigen);
				if(resultado) {
					destino.sumarDinero(dineroDestino);
				}
			}
		}
		return resultado;
	}
	
	private Billetera buscarBilletera(Integer cvu) {
		Billetera resultado = null;
		int i = 0;
		while(resultado == null && i < billeteras.size()) {
			if(this.billeteras.get(i).getCvu() == cvu) {
				resultado = this.billeteras.get(i);
			}
			i++;
		}
		return resultado;
	}
	
	private boolean existeBilletera(Integer cvu) {
		return this.buscarBilletera(cvu) != null;
	}
	
	
	
}
