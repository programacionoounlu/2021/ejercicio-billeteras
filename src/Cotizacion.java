import java.util.Objects;

public class Cotizacion {
	private Moneda monedaOrigen;
	
	private Moneda monedaDestino;
	
	private Double tasa;
	

	public Cotizacion(Moneda monedaOrigen, Moneda monedaDestino, Double tasa) {
		super();
		this.monedaOrigen = monedaOrigen;
		this.monedaDestino = monedaDestino;
		this.tasa = tasa;
	}

	public Moneda getMonedaOrigen() {
		return monedaOrigen;
	}

	public Moneda getMonedaDestino() {
		return monedaDestino;
	}

	public Double getTasa() {
		return tasa;
	}

	@Override
	public int hashCode() {
		return Objects.hash(monedaDestino, monedaOrigen, tasa);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cotizacion other = (Cotizacion) obj;
		return monedaDestino == other.monedaDestino && monedaOrigen == other.monedaOrigen
				&& Objects.equals(tasa, other.tasa);
	}
	
}
