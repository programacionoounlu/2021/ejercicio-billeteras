
public class Test {

	public static void main(String[] args) {
		Banco banco = new Banco();
		banco.agregarCotizacion(Moneda.PESOS, Moneda.DOLARES, 0.01);
		banco.agregarCotizacion(Moneda.EUROS, Moneda.DOLARES, 1.25);
		banco.agregarBilltera(1, "Perez", "Juan", new Dinero(Moneda.PESOS, 200.00));
		banco.agregarBilltera(2, "Rodriguez", "Julieta", new Dinero(Moneda.DOLARES));
		banco.agregarBilltera(3, "Mendez", "Ana", new Dinero(Moneda.EUROS));
		banco.agregarBilltera(4, "Mendez", "Julio", new Dinero(Moneda.EUROS));
		
		System.out.println("Juan le transfiere a Julieta ARS 100.00");
		banco.transferir(1, 2, 100.00);
		System.out.println("Saldo de Juan: " + banco.obtenerSaldoBilletera(1));
		System.out.println("Saldo de Julieta: " + banco.obtenerSaldoBilletera(2));
		
		System.out.println("Julieta le transfiere a Ana USD 1.00");
		banco.transferir(2, 3, 1.00);
		System.out.println("Saldo de Julieta: " + banco.obtenerSaldoBilletera(2));
		System.out.println("Saldo de Ana: " + banco.obtenerSaldoBilletera(3));
		
		System.out.println("Ana le transfiere a Julio EUR 0.80");
		banco.transferir(3, 4, 0.80);
		System.out.println("Saldo de Ana: " + banco.obtenerSaldoBilletera(3));
		System.out.println("Saldo de Julio: " + banco.obtenerSaldoBilletera(4));
	}

	

}
