import java.util.HashMap;
import java.util.Map;

public class Cotizador {
	private Map<Moneda, Map<Moneda, Cotizacion>> cotizaciones;
	
	public Cotizador() {
		super();
		this.cotizaciones = new HashMap<>();
	}

	private boolean existeCotizacion(Moneda de, Moneda a) {
		return this.cotizaciones.containsKey(de) && this.cotizaciones.get(de).containsKey(a);
	}
	
	public Cotizacion obtenerCotizacion(Moneda de, Moneda a){
		if(this.existeCotizacion(de, a)){
			return this.cotizaciones.get(de).get(a);
		} else if(this.existeCotizacion(a, de)) {
			return new Cotizacion(a, de, 1 / this.cotizaciones.get(a).get(de).getTasa());
		} else {
			return null;
		}
	}
	
	public boolean puedeCotizar(Moneda de, Moneda a) {
		return this.existeCotizacion(de, a) || this.existeCotizacion(a, de);
	}
	
	public void agregarCotizacion(Cotizacion c) {
		if(!this.cotizaciones.containsKey(c.getMonedaOrigen())) {
			this.cotizaciones.put(c.getMonedaOrigen(), new HashMap<>());
		}
		if(!this.cotizaciones.get(c.getMonedaOrigen()).containsKey(c.getMonedaDestino())){
			this.cotizaciones.get(c.getMonedaOrigen()).put(c.getMonedaDestino(), c);
		}
	}
	
	public Dinero convertir(Dinero origen, Moneda destino) {
		Dinero resultado = null;
		if(this.puedeCotizar(origen.getMoneda(), destino)) {
			Cotizacion cotizacion = this.obtenerCotizacion(origen.getMoneda(), destino);
			resultado = new Dinero(destino, origen.getMonto());
			resultado = resultado.mutiplicar(cotizacion.getTasa());
		}
		return resultado;
	}
	
}
