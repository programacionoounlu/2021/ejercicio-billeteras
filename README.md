# Resolución al ejercicio de las billeteras

Se requiere desarrollar una aplicación que permita transferir dinero de una billetera virtual a otra. Una billetera pertenece a un único usuario y, mediante la misma puede, se enviar y recibir dinero en diferentes monedas de otras billeteras.

![Diagrama de clases](diagrama.png "Diagrama de clases")
